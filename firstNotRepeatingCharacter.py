def firstNotRepeatingCharacter(s):
    dict = {}
    for i in range(0,len(s)):
        if s[i] in dict :
            dict[s[i]]=[2,0]
        else:    
            dict[s[i]]=[1,i]
    min = len(s)
    w = '_'
    for k in dict:
        v=dict[k]
        if(v[0]==1):
            return s[v[1]]
    return w            
  
