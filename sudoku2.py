def sudoku2(grid):
    dict={}
    dict_col={}
    for j in range(len(grid)):
        line = grid[j]
        for i in range(0,10):
            dict[i]=0
            dict_col[i]=0
        for i in range(len(line)):
            #line check
            if(line[i]!='.'):
                dict[int(line[i])]+=1
                if (dict[int(line[i])]>1): return False
            #column check    
            if(grid[i][j]!='.'):
                dict_col[int(grid[i][j])]+=1
                if (dict_col[int(grid[i][j])]>1): return False
    #square 3x3 check            
    for indexCol in range(3):
        for indexRow in range(3):
            for i in range(0,10):
                dict[i]=0
                
            for j in range(3):
                for i in range(3):
                    if(grid[indexCol*3+j][indexRow*3+i]!='.'):
                        dict[int(grid[indexCol*3+j][indexRow*3+i])]+=1
                        if(dict[int(grid[indexCol*3+j][indexRow*3+i])]>1):return False
           
    return True    

                
            